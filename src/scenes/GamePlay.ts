import 'phaser';
import CONFIG from "../../config";

let gameOptions = {

    // number of columns
    columns: 5,

    // number of rows, must be high enough to allow object pooling
    rows: 20,

    // tile speed in pixels per second
    tileSpeed: 200,

    localStorageName: "brickScore"
}

const WATING = 0;
const PLAY = 1;

export default class GamePlay extends Phaser.Scene{
    tileGroup : any;
    tileSize : number;
    middleColor : any;
    player : any;
    score : number;
    scoreText : Phaser.GameObjects.BitmapText;
    canMove : boolean;
    matched : boolean;
    gameState: number;
    bestScore: number;
    logo: Phaser.GameObjects.Sprite;
    bestScoreText: Phaser.GameObjects.BitmapText;
    playButton: Phaser.GameObjects.Sprite;

    constructor(){
        super("GamePlay");
		this.gameState = WATING;
    }
    preload(){
        this.load.spritesheet("tiles", "assets/sprites/tiles.png", {
            frameWidth: 100,
            frameHeight: 100
        });

		this.load.image("gameOver", "assets/sprites/GameOver.png");
		this.load.image("logo", "assets/sprites/Banner_hifpt.png");
        this.load.image("play", "assets/sprites/play.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
    }
    create(){
        this.score = 0;
        this.bestScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);
        
        if(this.gameState == PLAY){
            // physics group which manages all tiles in game
            this.tileGroup = this.physics.add.group()

            // determining tile size according to game width and columns
            this.tileSize = this.scale.width / gameOptions.columns;

            // time to add tiles to the game
            for(let i = 0; i < gameOptions.rows; i++){

                // build an array with integers between 0 and gameOptions.columns - 1: [0, 1, 2, ..., gameOptions.columns - 1]
                let values = Phaser.Utils.Array.NumberArrayStep(0, gameOptions.columns, 1);

                // then we shuffle the array
                Phaser.Utils.Array.Shuffle(values);

                // save middle column color of first row
                if(i == 0){
                    this.middleColor = values[Math.floor(gameOptions.columns / 2)];
                }

                // now we place the tiles, row by row
                for(let j = 0; j < gameOptions.columns; j++){

                    // add a tile. Tile frame is set according to "values" shuffled array
                    let tile = this.tileGroup.create(j * this.tileSize, i * this.tileSize + this.scale.height / 4 * 3, "tiles", values[j]);

                    // call adjustTile method to adjust tile origin and display size
                    this.adjustTile(tile);
                }
            }

            // let's build once again an array with integers between 0 and gameOptions.columns - 1
            let values = Phaser.Utils.Array.NumberArrayStep(0, gameOptions.columns, 1);

            // remove the item at "middlecolor" position because we don't want it to be randomly selected
            values.splice(this.middleColor, 1);

            // add the player to the game. Player color is picked amoung "values" array items, which does not contain anymore "middlecolor" value
            this.player = this.tileGroup.create(this.tileSize * Math.floor(gameOptions.columns / 2), this.scale.height / 4 * 3 - this.tileSize, "tiles", Phaser.Utils.Array.GetRandom(values));

            // adjust player origin and display size
            this.adjustTile(this.player);

            // the score
            this.score = 0;

            // move entire tile group up by gameOptions.tileSpeed pixels / second
            this.tileGroup.setVelocityY(-gameOptions.tileSpeed);

            // can the player move? Yes, at the moment
            this.canMove = true;

            // did we match any tile? No, at the moment
            this.matched = false;

            // wait for player input
            this.input.on("pointerdown", this.moveTile, this);
        }
        else{
            var text = this.add.text(this.scale.width/2 , this.scale.height/4, "Welcome to", {
				font: "bold 60px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            var text = this.add.text(this.scale.width/2 , this.scale.height/4+100, "Bricks Game", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
			this.playButton.setScale(1)
			this.playButton.setOrigin(0.5,0.5);
			this.playButton.setInteractive();
			this.playButton.on("pointerdown", function(){
				this.scene.start("GamePlay");
				this.gameState = PLAY;
			}, this)
        }
    }
    updateScore(){
        this.score++;
        if(this.score > this.bestScore){
            this.bestScore = this.score;
            this.bestScoreText.text = this.bestScore.toString();
        }
        this.scoreText.text = this.score.toString();

        if(this.score % 10 == 0){
            gameOptions.tileSpeed++;
            this.tileGroup.setVelocityY(-gameOptions.tileSpeed);
        }
    }

    gameOver(){
        this.physics.destroy();
        // shake the camera
        this.cameras.main.shake(500, 0.01);

        this.gameState = WATING;
        
        localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());
        var gameOver = this.add.sprite(this.scale.width/2, this.scale.height/2, "gameOver");
        gameOver.setScale(1)
        gameOver.setOrigin(0.5,0.5);
        // restart the game in two seconds
        this.time.addEvent({
            delay: 4000,
            callbackScope: this,
            callback: function(){
                this.scene.start("GamePlay")
            }
        });
    }

    // method to set tile origin and display size
    adjustTile(sprite){
        if(this.gameState == PLAY){
            // set origin at the top left corner
            sprite.setOrigin(0);

            // set display width and height to "tileSize" pixels
            sprite.displayWidth = this.tileSize;
            sprite.displayHeight = this.tileSize;
        }
    }

    // method to move player tile
    moveTile(pointer){

        // if we can move...
        if(this.canMove && this.gameState == PLAY){

            // determine column according to input coordinate and tile size
            let column = Math.floor(pointer.x / this.tileSize);

            // get the ditance from current player tile and destination
            let distance = Math.floor(Math.abs(column * this.tileSize - this.player.x) / this.tileSize);

            // did we actually move?
            if(distance > 0){

                // we can't move anymore
                this.canMove = false;

                // tween the player to destination tile
                this.tweens.add({
                    targets: [this.player],
                    x: column * this.tileSize,
                    duration: distance * 30,
                    callbackScope: this,
                    onComplete: function(){

                        // at the end of the tween, check for tile match
                        this.checkMatch();
                    }
                });
            }
        }
    }

    // method to check tile matches
    checkMatch(){
        if(this.gameState == PLAY){
            // get tile below player tile
            let tileBelow = this.physics.overlapRect(this.player.x + this.tileSize / 2, this.player.y + this.tileSize * 1.5, 1, 1);

            // "tileBelow" is an array so we have to compare the first - and only - item frame with player frame. Are the two frames the same?
            let v = tileBelow[0].gameObject as any;
            if(v.frame.name == this.player.frame.name){

                // we have a match
                this.matched = true;

                // check the whole row below player tile
                let rowBelow = this.physics.overlapRect(0, this.player.y + this.tileSize * 1.5, this.scale.width, 1);

                // tween down the player
                this.tweens.add({
                    targets: [this.player],
                    y: v.y,
                    duration: 100,
                    callbackScope: this,
                    onUpdate: function(tween, target){

                        // at each update, we have to adjust player position because tiles continue moving up
                        let v = tileBelow[0].gameObject as any;
                        this.player.y = Math.min(this.player.y, v.y)
                    },

                    // at the end of the tween, we have to move the row at the bottom, to reuse sprites
                    onComplete: function(){

                        // increment score
                        this.updateScore()

                        // the good old array with all integers from zero to gameOptions.columns - 1
                        let values = Phaser.Utils.Array.NumberArrayStep(0, gameOptions.columns, 1);

                        // let's shuffle the array
                        Phaser.Utils.Array.Shuffle(values);

                        // place all tiles below the lowest row
                        for(let i = 0; i < gameOptions.columns; i++){
                            let v = rowBelow[i].gameObject as any;
                            v.setFrame(values[i]);
                            v.y += this.tileSize * gameOptions.rows;
                        }

                        // check for matches again, there could be a combo
                        this.checkMatch();
                    }
                });
            }

            // what to do when player moved but there isn't any match?
            else{

                // we can move again
                this.canMove = true;

                // is there a previous match? Did we come here from a previous match?
                if(this.matched){

                    // no more matches
                    this.matched = false;

                    // get the tile below the player
                    let tileBelow = this.physics.overlapRect(this.player.x + this.tileSize / 2, this.player.y + this.tileSize * 1.5, 1, 1);

                    // the good old array with all integers from zero to gameOptions.columns - 1
                    let values = Phaser.Utils.Array.NumberArrayStep(0, gameOptions.columns, 1);

                    // remove the item at "frame" value of tile below the player pbecause we don't want it to be randomly selected
                    let v = tileBelow[0].gameObject as any;
                    values.splice(v.frame.name, 1);

                    // change player frame
                    this.player.setFrame(Phaser.Utils.Array.GetRandom(values));
                }
            }
        }
    }

    // method to be executed at each frame
    update(){
        if(this.gameState == PLAY){
            // if the player touches the top of the screen...
            if(this.player.y < 120){

                // game over man, restart the game
                this.gameOver();
            }
        }
    }
}

